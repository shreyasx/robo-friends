import Image from "next/image";

export async function getStaticPaths() {
	const res = await fetch("https://jsonplaceholder.typicode.com/users");
	const robos = await res.json();
	const paths = robos.map(robo => ({ params: { id: robo.id.toString() } }));
	return { paths, fallback: false };
}

export async function getStaticProps(context) {
	const response = await fetch(
		`https://jsonplaceholder.typicode.com/users?id=${context.params.id}`
	);
	const robot = await response.json();
	return { props: { robot: robot[0] } };
}

const Robot = props => {
	return (
		<div style={{ textAlign: "center" }}>
			<h1>{props.robot.name}</h1>
			<Image
				width={300}
				height={300}
				alt="robots"
				src={`https://robohash.org/${props.robot.id}?500x500`}
			/>
			<h2 style={{ color: "white", marginTop: "50px" }}>
				Email: {props.robot.email}
			</h2>
		</div>
	);
};

export default Robot;

// ON EVERY REQUEST
// export async function getServerSideProps(context) {
// 	console.log("getServerSideProps");
// 	const response = await fetch(
// 		`https://jsonplaceholder.typicode.com/users?id=${context.query.id}`
// 	);
// 	const robot = await response.json();
// 	return { props: { robot: robot[0] } };
// }
